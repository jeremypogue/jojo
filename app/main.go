package app

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type SendSMSResponse struct {
	Sid                 string      `json:"sid"`
	DateCreated         string      `json:"date_created"`
	DateUpdated         string      `json:"date_updated"`
	DateSent            interface{} `json:"date_sent"`
	AccountSid          string      `json:"account_sid"`
	To                  string      `json:"to"`
	From                string      `json:"from"`
	MessagingServiceSid interface{} `json:"messaging_service_sid"`
	Body                string      `json:"body"`
	Status              string      `json:"status"`
	NumSegments         string      `json:"num_segments"`
	NumMedia            string      `json:"num_media"`
	Direction           string      `json:"direction"`
	APIVersion          string      `json:"api_version"`
	Price               interface{} `json:"price"`
	PriceUnit           string      `json:"price_unit"`
	ErrorCode           interface{} `json:"error_code"`
	ErrorMessage        interface{} `json:"error_message"`
	URI                 string      `json:"uri"`
	SubresourceUris     struct {
		Media string `json:"media"`
	} `json:"subresource_uris"`
}

var (
	endpointURL = "https://api.twilio.com/2010-04-01/Accounts/%s/Messages.json"
	accountSid  = os.Getenv("TWILIO_ACCOUNT_SID")
	authToken   = os.Getenv("TWILIO_AUTH_TOKEN")
	from        = os.Getenv("TWILIO_FROM_PHONE")
)

func SendRequest(to string, body string) (SendSMSResponse, error) {
	smsResponse := SendSMSResponse{}
	msgData := url.Values{}
	msgData.Set("To", to)
	msgData.Set("From", from)
	msgData.Set("Body", body)
	msgDataReader := *strings.NewReader(msgData.Encode())

	client := http.Client{
		Timeout: time.Duration(5 * time.Second),
	}
	req, newReqErr := http.NewRequest(
		"POST",
		fmt.Sprintf(endpointURL, accountSid),
		&msgDataReader,
	)
	if newReqErr != nil {
		return smsResponse, newReqErr
	}
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(accountSid, authToken)

	resp, requestErr := client.Do(req)
	defer req.Body.Close()
	if requestErr != nil {
		return smsResponse, requestErr
	}

	decodeErr := json.NewDecoder(resp.Body).Decode(&smsResponse)
	if decodeErr != nil {
		return smsResponse, decodeErr
	}

	return smsResponse, nil
}
